-- CreateTable
CREATE TABLE "Laporan" (
    "id" SERIAL NOT NULL,
    "namaPelapor" TEXT NOT NULL,
    "nomorPelapor" INTEGER NOT NULL,
    "gambar" TEXT NOT NULL,
    "lokasi" TEXT NOT NULL,
    "deskripsi" TEXT NOT NULL,

    CONSTRAINT "Laporan_pkey" PRIMARY KEY ("id")
);
