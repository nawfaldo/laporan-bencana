const { gql } = require('apollo-server');

const typeDefs = gql`
  type Laporan {
    id: ID!
    namaPelapor: String!
    nomorPelapor: Int!
    gambar: String!
    lokasi: String!
    deskripsi: String!
  }

  type Query {
    semuaLaporan: [Laporan!]
  }

  type Mutation {
    lapor(
      namaPelapor: String!
      nomorPelapor: Int!
      gambar: String!
      lokasi: String!
      deskripsi: String!
    ): Laporan!
    editLaporan(
      id: String!
      namaPelapor: String!
      nomorPelapor: Int!
      gambar: String!
      lokasi: String!
      deskripsi: String!
    ): Laporan!
    deleteLaporan(id: ID!): Laporan!
  }
`;

module.exports = {
  typeDefs,
};
