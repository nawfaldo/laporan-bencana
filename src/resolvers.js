const { prisma } = require('./database.js');

const Laporan = {
  id: (parent, args, context, info) => parent.id,
  namaPelapor: (parent) => parent.namaPelapor,
  nomorPelapor: (parent) => parent.nomorPelapor,
  gambar: (parent) => parent.gambar,
  lokasi: (parent) => parent.lokasi,
  deskripsi: (parent) => parent.deskripsi,
};

const Query = {
  semuaLaporan: async () => {
    const result = await prisma.laporan.findMany({});
    return result;
  },
};

const Mutation = {
  lapor: async (
    parent,
    { namaPelapor, nomorPelapor, gambar, lokasi, deskripsi }
  ) => {
    const result = await prisma.laporan.create({
      data: {
        namaPelapor: namaPelapor,
        nomorPelapor: nomorPelapor,
        gambar: gambar,
        lokasi: lokasi,
        deskripsi: deskripsi,
      },
    });

    return result;
  },
  editLaporan: async (parent, args) => {
    const result = await prisma.laporan.update({
      where: {
        id: Number(args.id),
      },
      data: {
        namaPelapor: args.namaPelapor,
        nomorPelapor: args.nomorPelapor,
        gambar: args.gambar,
        lokasi: args.lokasi,
        deskripsi: args.deskripsi,
      },
    });

    return result;
  },
  deleteLaporan: async (parent, args) => {
    const result = await prisma.laporan.delete({
      where: {
        id: Number(args.id),
      },
    });
    return result;
  },
};

const resolvers = { Laporan, Query, Mutation };

module.exports = {
  resolvers,
};
